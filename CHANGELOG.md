# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

### Fixed


## [24.6.2] - 2024-11-26

### Added

- example of RIXS plane extraction
- `famewoks.utils`
- `famewoks.controller`: initial version

### Changed

- `get_y` moved in `famewoks.utils`

### Fixed

- fixed [#13](https://gitlab.esrf.fr/F-CRG/fames/famewoks/-/issues/13)


## [24.5.3] - 2024-07-01

- new data model

## [24.4.0] - 2024-05-03

- first stable release