# -*- coding: utf-8 -*-
#
# This file is part of the famewoks project
#
# Copyright (c) Mauro Rovezzi, CNRS
# Distributed under the GNU GPLv3. See LICENSE for more info.

"""
How the data are memory mapped
"""

from dataclasses import dataclass, field
from typing import List, Union, Any, Mapping
import numpy as np
from larch import Group


@dataclass
class ExpCounters:
    ene: str  #: energy counter
    ix: List[str]  #: I0, I1, I2 counters
    fluo_roi: List[str]  #: fluorescence counters for ROI
    fluo_corr: List[str]  #: ROI corrected by dead time
    fluo_time: List[str]  #: fluorescence counters timer
    time: str  #: time


def xas_scan_default_dict():
    """Define default dictionary for XasScan.data/flags/names"""
    return {
        "fluos": None,
        "ix": None,
        "mufluo": None,
        "mutrans": None,
        "muref": None,
    }


# DATA_NAMES = tuple(xas_scan_default_dict().keys())
DATA_NAMES = ("fluos", "fluo", "trans", "ref")


# follows XDI column namespace
# https://github.com/XraySpectroscopy/XAS-Data-Interchange/blob/master/specification/dictionary.md#defined-items-in-the-column-namespace
@dataclass
class XasScan:
    """*DEPRECATED*"""

    def __init__(self, **kws):
        self.__dict__.update(kws)

    flag: int
    fname: str
    scanno: str
    scanint: int
    title: str
    time: str
    comment: str
    energy: Union[np.ndarray[np.float64, Any], None]
    data: Mapping[str, Union[List[np.ndarray[np.float64, Any]], None]] = field(
        default_factory=xas_scan_default_dict
    )
    flags: Mapping[str, Union[List[int], None]] = field(
        default_factory=xas_scan_default_dict
    )
    names: Mapping[str, Union[List[str], None]] = field(
        default_factory=xas_scan_default_dict
    )
    # ix: Union[np.ndarray, None]  #: data beam intensity
    # ix_flags: Union[List[int], None]
    # ix_names: Union[List[str], None]
    # fluos: Union[np.ndarray, None]  #: data fluorescence
    # fluos_flags: Union[List[int], None]  #: flags for good/bad fluorescence channels
    # fluos_names: Union[List[str], None]  #: labels for the fluorescence channels
    # mutrans: Union[np.ndarray, None]
    # muref: Union[np.ndarray, None]  #: np.ln
    # mufluo: Union[np.ndarray, None]  #: Sum(good fluos)/I0
    # journal: List


@dataclass
class XasScanGroup:
    """Version of XasScan with Larch Groups"""

    flag: int
    fname: str
    sample: str
    dataset: str
    scanno: str
    scanint: int
    title: str
    time: str
    comment: str
    fluos: Union[List[Group], None]
    fluo: Union[List[Group], None]
    trans: Union[List[Group], None]
    ref: Union[List[Group], None]

    @property
    def name(self):
        return f"{self.sample}/{self.dataset}/{self.scanno}"

@dataclass
class ExpDataset:
    name: str
    flag: int
    scans: List[XasScanGroup]  #: List of XasScan objects
    scans_names: List[int]
    scans_emin: np.ndarray[np.float64, Any]
    scans_emax: np.ndarray[np.float64, Any]
    scans_eshifts: List[float]
    energy: Union[np.ndarray[np.float64, Any], None]
    trans: Union[np.ndarray[np.float64, Any], None]
    fluo: Union[np.ndarray[np.float64, Any], None]
    ref: Union[np.ndarray[np.float64, Any], None]


@dataclass
class ExpSample:
    name: str
    flag: int
    datasets: Union[List[ExpDataset], None]


@dataclass
class ExpSession:
    flag: int
    datadir: str
    proposal: str
    session: str
    proposer: str
    lc: str  #: local contact initials (e.g. "OP, IK")
    elem: str
    edge: str
    comment: str
    counters: ExpCounters
    samples: List[ExpSample]  #: List of ExpSample
    bad_samples: List[str]
    bad_fluo_channels: Union[List[int], None]
    enealign: Union[Group, None] #: Energy alignment group

    def __post_init__(self):
        pass

    def load_settings(self, fname):
        """Load settings from file"""
        raise NotImplementedError

    def save_settings(self, fname):
        """Save settings to file"""
        raise NotImplementedError


### DEFAULTS ###

# ESRF/BM16 counters names for fluorescence detector with XGLAB electronics
CNTS_FLUO = ExpCounters(
    ene="energy_enc",
    ix=["p201_1_bkg_sub", "p201_3_bkg_sub", "p201_5_bkg_sub"],
    fluo_roi=[
        f"xglab_det{ich}_roi1" for ich in range(0, 16)
    ],  # all detector names for ROI1
    fluo_corr=[
        f"roi1_corr_dtc_det{ich}" for ich in range(0, 16)
    ],  # all detector names (DT corrected)
    fluo_time=[
        f"xglab_det{ich}_elapsed_time" for ich in range(0, 16)
    ],  # elapsed time, which is different for the spikes
    time="sec",  # "musst_timer"
)

# ESRF/BM16 counters names for fluorescence detector with XMAP electronics
CNTS_FLUO_XMAP = ExpCounters(
    ene="energy_enc",
    ix=["p201_1_bkg_sub", "p201_3_bkg_sub", "p201_5_bkg_sub"],
    fluo_roi=[
        f"xmapd16_det{ich}_roi1" for ich in range(0, 16)
    ],  # all detector names for ROI1
    fluo_corr=[
        f"roi1_corr_dtc_det{ich}" for ich in range(0, 16)
    ],  # all detector names (DT corrected)
    fluo_time=[
        f"xmapd16_det{ich}_elapsed_time" for ich in range(0, 16)
    ],  # elapsed time, which is different for the spikes
    time="sec",  # "musst_timer"
)


# ESRF/BM16 counters names for the spectrometer
CNTS_CAS = ExpCounters(
    ene="energy_enc",
    ix=["p201_1_bkg_sub", "p201_3_bkg_sub", "p201_5_bkg_sub"],  #: I0, I1, I2
    fluo_roi=["mercury4_det2_roi1"],  # all detector names for ROI1
    fluo_corr=["roi1_corr_det2"],  # all detector names (DT corrected)
    fluo_time=[
        "mercury4_det2_elapsed_time"
    ],  # elapsed time, which is different for the spikes
    time="sec",  # "musst_timer"
)

# ESRF/BM16 counters names for the spectrometer with XPAD detector
CNTS_CAS_XPAD = ExpCounters(
    ene="energy_enc",
    ix=["p201_1_bkg_sub", "p201_3_bkg_sub", "p201_5_bkg_sub"],  #: I0, I1, I2
    fluo_roi=["xpad_roi1"],  # all detector names for ROI1
    fluo_corr=["xpad_roi1"],  # all detector names (DT corrected)
    fluo_time=[
        "sec"
    ],  # elapsed time, which is different for the spikes
    time="sec",  # "musst_timer"
)
