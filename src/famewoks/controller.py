# -*- coding: utf-8 -*-
#
# This file is part of the famewoks project
#
# Copyright (c) Mauro Rovezzi, CNRS
# Distributed under the GNU GPLv3. See LICENSE for more info.

"""
Controller for the ExpSession
"""

from typing import List, Tuple, Optional, Dict, Any
from pathlib import Path

from larch.utils import logging
from .datamodel import ExpSession, ExpSample
from .bliss2larch import (
    search_samples,
    search_datasets,
    show_samples_info,
    show_datasets_info,
    load_data,
)

_logger = logging.getLogger("ExpSessCtrl")
_logger.setLevel("INFO")  # adjust logger level .DEBUG, .INFO, .WARNING, .ERROR


class ExpSessionController:
    def __init__(self, session: ExpSession):
        self._session = session

    @property
    def session(self) -> ExpSession:
        return self._session

    @session.setter
    def session(self, value: ExpSession):
        self._session = value

    @property
    def samples(self, all: bool = False) -> None:
        return show_samples_info(self._session, all=all, show_datasets=False)

    def build_data_tree(
        self,
        verbose: bool = False,
        ignore_names: List[str] = ["rack", "mount", "align", "bl_"],
    ):
        """Build the whole data tree from scratch"""
        allsamples = search_samples(self.session, verbose=verbose, ignore_names=ignore_names)
        errors = []
        for samp in allsamples:
            alldatasets = search_datasets(self.session, sample=samp.name, verbose=False)
            if alldatasets is None:
                continue
            for dset in alldatasets:
                try:
                    load_data(self.session, dset, skip_scans=[], merge=True, calc_eshift=False)
                except Exception:
                    errors.append(dset.name)
                    continue
        _logger.warning(f"Found errors in {errors}")
