# -*- coding: utf-8 -*-
#
# This file is part of the famewoks project
#
# Copyright (c) Mauro Rovezzi, CNRS
# Distributed under the GNU GPLv3. See LICENSE for more info.
"""
Generic utilities
=================
"""

from typing import Union
import numpy as np
from larch import Group


def get_y(
    grp: Group,
    yattr: str = "mu",
    ynorm: Union[bool, str, None] = None,
    ylabel: Union[str, None] = None,
    show_deriv: bool = False,
) -> tuple:
    """Get the y-data for a given XAFS data Group."""
    try:
        y = getattr(grp, yattr)
    except AttributeError:
        return (None, None)
    if ylabel is None:
        try:
            ylabel = grp.glabel
        except AttributeError:
            ylabel = grp.filename
        finally:
            ylabel = ""
    if ynorm is True:
        y = grp.norm
        ylabel = f"{ylabel} (norm)"
    if ynorm == "flat":
        y = grp.flat
        ylabel = f"{ylabel} (flat)"
    if ynorm == "area":
        y = y / np.trapz(y)
        ylabel = f"{ylabel} (norm by area)"
    if show_deriv:
        y = grp.dmude
        ylabel = f"{ylabel} (dmude)"
    return (y, ylabel)
