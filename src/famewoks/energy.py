import numpy as np

from lmfit import Parameters, minimize
from larch import Group
from larch.utils import logging
from larch.math import (
    index_of,
    interp,
    smooth,
)
from larch.xafs import find_energy_step, pre_edge

_logger = logging.getLogger("WKFL")
_logger.setLevel("INFO")  # adjust logger level .DEBUG, .INFO, .WARNING, .ERROR


def ensure_en_orig(dgroup):  # type: ignore
    if not hasattr(dgroup, "energy_orig"):
        dgroup.energy_orig = dgroup.energy[:]


def energy_align(
    dat: Group,
    ref: Group,
    emin: float = -15,
    emax: float = 35,
    array: str = "dmude",
    eshift_max: float = 5,
):
    if not (hasattr(dat, "energy") or hasattr(ref, "energy")):
        raise ValueError("dat/ref groups must have 'energy' attribute")
    if not (hasattr(dat, array) or hasattr(ref, array)):
        raise ValueError(f"dat/ref groups must have '{array}' attribute")

    ensure_en_orig(dat)
    ensure_en_orig(ref)

    if not hasattr(dat, "e0"):
        pre_edge(dat)
    if not hasattr(ref, "e0"):
        pre_edge(ref)

    #if not hasattr(dat, "dmude"):
    #    mu = getattr(dat, "norm", getattr(dat, "mu"))
    #    en = getattr(dat, "energy")
    #    dat.dmude = np.gradient(mu) / np.gradient(en)  # type: ignore

    dat.xdat = dat.energy_orig[:]  # type: ignore
    ref.xdat = ref.energy_orig[:]  # type: ignore
    estep = find_energy_step(dat.xdat)  # type: ignore
    if np.isnan(estep):
        estep = 0.1
    i1 = index_of(ref.energy_orig, ref.e0 + emin)  # type: ignore
    i2 = index_of(ref.energy_orig, ref.e0 + emax)  # type: ignore

    def resid(pars, ref, dat, i1, i2):  # type: ignore
        """Fit residual"""
        newx = dat.xdat + pars["eshift"].value
        scale = pars["scale"].value
        y = interp(newx, getattr(dat, array), ref.xdat, kind="cubic")
        return smooth(
            ref.xdat,
            y * scale - getattr(ref, array),
            xstep=estep,
            sigma=0.5,  # type: ignore
        )[i1:i2]

    params = Parameters()
    eshift_init = 0 if abs(ref.e0 - dat.e0) >= eshift_max else ref.e0 - dat.e0  # type: ignore
    params.add("eshift", value=eshift_init, min=-3 * eshift_max, max=3 * eshift_max)
    params.add("scale", value=estep, min=1.0e-8, max=50)
    try:
        result = minimize(resid, params, args=(ref, dat, i1, i2), max_nfev=1000)
        eshift = result.params["eshift"].value  # type: ignore
        yscale = result.params['scale'].value # type: ignore
        #if result.params["scale"].value > 1.15 or result.params["scale"].value < 0.90:
        #    eshift = np.nan
        _logger.debug(f"--> energy shift: {eshift} (yscale: {yscale})")
    except Exception:
        _logger.warning("cannot calculate the energy shift")
        eshift = np.nan

    return eshift
