# -*- coding: utf-8 -*-
#
# This file is part of the famewoks project
#
# Copyright (c) Mauro Rovezzi, CNRS
# Distributed under the GNU GPLv3. See LICENSE for more info.

import os
from famewoks import __version__ as wkflver
from famewoks.plots import plot_data
from famewoks.tests.datainfos import DATAINFOS, get_exp_session
from famewoks.bliss2larch import (
    search_samples,
    show_samples_info,
    search_datasets,
    load_data,
    set_bad_fluo_channels,
    set_bad_scans,
    set_bad_samples,
    merge_data,
    save_data,
    get_scan_type,
)
from famewoks.bliss2larch import _logger

if __name__ == "__main__":
    ### TEST ###
    _logger.setLevel("DEBUG")

    # show workflow version (and famewoks branch information)
    branch = os.popen("cd ~/devel/famewoks; git branch --show-current").read()[:-1]
    _logger.info(f"--> Worflow version: {wkflver} ({branch})")
    if 1:
        """
        Test save_data() by data channels with dynamic scan selection
        """
        session = get_exp_session(DATAINFOS, proposer="Isaure", session="20241003")
        samples = search_samples(session)
        datasets = search_datasets(session, sample="bHgS_08pc_310", verbose=False)
        dataset = datasets[0]
        load_data(
            session,
            dataset,
            use_fluo_corr=False,
            iskip=5,
            istrip=1,
            calc_eshift=False,
            merge=False,
            skip_scans=[],
        )
        set_bad_scans(dataset, scans=None)
        set_bad_scans(dataset, scans="2, 9")
        save_data(dataset, data=["fluo"], datadir=None, save_rebinned=False)
        set_bad_scans(dataset, scans=None)
        set_bad_scans(dataset, scans="3:8")
        save_data(dataset, data=["ref"], datadir=None, save_rebinned=False)
    if 0:
        """
        Test bug fix in plotting when loading data without merge
        """
        session = get_exp_session(DATAINFOS, proposer="Sanchez", session="20240716")
        samples = search_samples(session)
        set_bad_samples(session, ["emptycell"])
        show_samples_info(session, all=True)
        datasets = search_datasets(session, sample=5)
        dataset = datasets[0]
        load_data(
            session,
            dataset,
            use_fluo_corr=True,
            iskip=1,
            calc_eshift=False,
            merge=False,
            skip_scans=[],
        )
        fig = plot_data(
            dataset,
            data="ref",
            ynorm="area",
            show_slide=True,
            show_i0=False,
            show_e0=False,
            show_merge=True,
        )
    if 0:
        """
        To test the energy shift with bad references
        """
        session = get_exp_session(DATAINFOS, proposer="Fernandez-Martinez")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=27)
        dset = datasets[2]
        load_data(session, dset, skip_scans=[], merge=False, calc_eshift=True)
    if 0:
        """
        To test the case of a single fluorescence channel that is being disabled by `set_bad_fluo_channels()`
        """
        session = get_exp_session(DATAINFOS, proposer="Ould-Chikh")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=28)
        dset = datasets[0]
        load_data(session, dset, skip_scans=[], merge=True, calc_eshift=True)
        set_bad_fluo_channels(dset, channels=[0], scan=[5])
        fig = plot_data(
            dset,
            data="fluos",
            ynorm="area",
            show_slide=True,
            show_i0=True,
            show_merge=True,
            show_e0=True,
        )

    if 0:
        session = get_exp_session(DATAINFOS, proposer="Thevenet")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=28)
        idset = 1
        dset = datasets[idset]
        load_data(session, dset, skip_scans=[], merge=True, calc_eshift=True)
        fig = plot_data(
            dset,
            data="fluo",
            ynorm="area",
            show_slide=True,
            show_i0=True,
            show_merge=True,
            show_e0=True,
        )
        save_data(dset, save_rebinned=False)
    if 0:  # bug
        session = get_exp_session(DATAINFOS, proposer="Stellato", session="20240625")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=32)
        idset = 7
        dset = datasets[idset]
        load_data(session, dset, skip_scans=[], merge=True, calc_eshift=False)
        fig = plot_data(
            dset,
            data="fluos",
            ynorm="area",
            show_slide=True,
            show_i0=False,
            show_merge=True,
            show_e0=True,
        )
        set_bad_fluo_channels(dset, channels=[0, 3, 15], scan=[1, 2])
        fig = plot_data(
            dset,
            data="fluos",
            ynorm="area",
            show_slide=True,
            show_i0=False,
            show_merge=True,
            show_e0=True,
        )

    if 0:
        session = get_exp_session(DATAINFOS, proposer="Stellato", session="20240625")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=27)
        idset = 7
        dset = datasets[idset]
        load_data(
            session, dset, skip_scans=[4], merge=True, calc_eshift=False
        )  # wrong scan : 4
        fig = plot_data(
            dset,
            data="ref",
            ynorm=None,
            show_slide=True,
            show_i0=False,
            show_merge=True,
            show_e0=True,
        )
        save_data(dset, save_rebinned=True)

    if 0:
        session = get_exp_session(DATAINFOS, proposer="Fernandez-Martinez")
        samples = search_samples(session)
        datasets = search_datasets(session, sample=27)
        idset = 2
        dset = datasets[idset]
        load_data(session, dset, skip_scans=[1, 32, 37], merge=True, calc_eshift=False)
        fig = plot_data(
            dset,
            data="fluo",
            ynorm="area",
            show_slide=False,
            show_i0=True,
            show_merge=False,
            show_e0=True,
        )
        save_data(dset, save_rebinned=True)

    if 0:
        session = get_exp_session(DATAINFOS, proposer="Fernandez-Martinez")
        samples = search_samples(session)
        isamp = 4
        samples[isamp]
        datasets = search_datasets(session, sample=isamp)
        idset = 1
        dset = datasets[idset]
        load_data(session, dset, skip_scans=[1, 6], merge=False, calc_eshift=False)
        fig = plot_data(
            dset,
            data="fluos",
            ynorm="area",
            show_slide=True,
            show_i0=True,
            show_merge=False,
            show_e0=False,
        )
        save_data(dset, save_rebinned=True)

    if 0:
        session = get_exp_session(DATAINFOS, proposer="Daval")
        isamp, idset, wscans = 17, 0, [6]
        samples = search_samples(session)
        datasets = search_datasets(session, sample=isamp)
        dset = datasets[idset]
        load_data(session, dset, wrong_scans=wscans)
        fig = plot_data(
            datasets[idset],
            data="ref",
            ynorm=None,
            show_slide=True,
            show_i0=False,
            show_merge=True,
        )
        # set_bad_fluo_channels(dset, channels=None, scan=wscans)
        # set_bad_fluo_channels(dset, channels=session.bad_fluo_channels, scan=wscans)

    if 0:
        session = get_exp_session(DATAINFOS)
        isamp, idset, wscans = 3, 1, [1]
        samples = search_samples(session)
        datasets = search_datasets(session, sample=isamp)
        dset = datasets[idset]
        load_data(session, dset, wrong_scans=wscans)
        # plot_data(datasets[idset], plot_data="mufluo")
        # set_bad_fluo_channels(dset, channels=None, scan=wscans)
        set_bad_fluo_channels(dset, channels=session.bad_fluo_channels, scan=wscans)

    if 0:
        from datainfos import DATAINFOS_OLD as DATAINFOS
        from datainfos import get_infodict

        datadir, samples, datas, dataset, counters = workflow(get_infodict(DATAINFOS))

    if 0:
        # TODO: import/export in YAML
        from ruamel.yaml import YAML

        yaml = YAML(typ="safe")
        yaml.register_class(ExpCounters)
        with open("datainfos.yml", "w") as f:
            yaml.dump(DATAINFOS, f)
