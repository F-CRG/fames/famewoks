# Famewoks

Workflows for the French CRG FAME beamlines, FAME-UHD (BM16) and FAME-PIX (BM30) at the ESRF.

## Documentation

[https://famewoks.readthedocs.io](https://famewoks.readthedocs.io)
