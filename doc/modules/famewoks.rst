famewoks package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   famewoks.tests

Submodules
----------

famewoks.bliss2larch module
---------------------------

.. automodule:: famewoks.bliss2larch
   :members:
   :undoc-members:
   :show-inheritance:

famewoks.datamodel module
-------------------------

.. automodule:: famewoks.datamodel
   :members:
   :undoc-members:
   :show-inheritance:

famewoks.plots module
---------------------

.. automodule:: famewoks.plots
   :members:
   :undoc-members:
   :show-inheritance:

famewoks.plots\_wip module
--------------------------

.. automodule:: famewoks.plots_wip
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: famewoks
   :members:
   :undoc-members:
   :show-inheritance:
