# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os, sys
from datetime import datetime

import famewoks

sys.path.insert(0, os.path.abspath(".."))

# check if building on readthedocs.org
ON_RTD = os.environ.get("READTHEDOCS") == "True"

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Famewoks"
author = "Mauro Rovezzi"
copyright = f"2023--{datetime.now().year}, {author}, CNRS, Grenoble"
release = famewoks.__version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.mathjax",
    "sphinx.ext.todo",
    "nbsphinx",
    "sphinx_copybutton",
    "nbsphinx_link",
    "myst_parser",
]

myst_enable_extensions = ["colon_fence"]

myst_config = {
    'numfig': True, # Enable numfig extension
    'numfig_format': '{num}',  # Format for figure numbers
    'numfig_suffix': 'Figure',  # Suffix for figure numbers
    'figure_caption_prefix': '',  # Prefix for figure captions
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# The suffix(es) of source filenames.
source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
    ".txt": "markdown",
}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# The theme to use for HTML and HTML Help pages. See the documentation for
# a list of builtin themes.
#html_theme = "sphinx_rtd_theme"
html_theme = "pydata_sphinx_theme"


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ["_static"]

autodoc_default_options = {
    "members": True,
    "undoc-members": True,
    "member-order": "bysource",
    "special-members": "__init__",
    "show-inheritance": True,
    "inherited-members": True,
}

nbsphinx_execute = "never"
nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc=figure.dpi=96",
]
nbsphinx_allow_errors = True

# https://github.com/sphinx-doc/sphinx/issues/12300#issuecomment-2062238457
suppress_warnings = ["config.cache"]

# Don't add .txt suffix to source files:
html_sourcelink_suffix = ""
