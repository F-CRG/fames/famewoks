# Local installation (e.g. on your own computer)

The code is currently (2025) under active development and is distributed as
source only. A minimum knowledge of Linux, Python and Jupyter is required.

## Minimal installation procedure for an Unix-like OS

The following procedure is given for an Unix-like OS (Linux, MacOS,
and Windows Subsystem for Linux - WSL).

**Windows users**: you can install Linux on your Windows machine by simply
installing the [Windows Subsystem for Linux -
WSL](https://learn.microsoft.com/en-us/windows/wsl/).

- Create a Conda python environment and activate it. You can use
  [miniforge](https://github.com/conda-forge/miniforge). Please, refer to the
  documentation how to install it. In a Linux-like shell (e.g Mac or Windows
  Substyem for Linux), this is as simple as:

```bash
#go to your local install directory
cd; mkdir local
cd local
wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh
bash ./Miniforge3-Linux-x86_64.sh -b -p miniforge -u
source miniforge/bin/activate
mamba create -n famewoks python=3.12
source conda/bin/activate famewoks
```

- Install [Larch](https://xraypy.github.io/xraylarch/installation.html#install-conda)

```bash
conda install -yc conda-forge xraylarch
```

- Install Git

```bash
mamba -y install git
```

- Install [JupyterLab](https://jupyter.org/)

```bash
pip install jupyterlab
```

- Install `famewoks`

```
pip install git+https://gitlab.esrf.fr/F-CRG/fames/famewoks.git
```

This installs the `main` branch of the code, which is the latest
*production/stable branch* that you have probably used on the beamline. If you
want to install a *specific branch*:

```
pip install -U git+https://gitlab.esrf.fr/F-CRG/fames/famewoks.git@the_name_of_the_branch
```

- Run JupyterLab: open a shell, activate the `famewoks` conda environment and start Jupyterlab

```bash
#go to the directory where the notebook is
conda activate famewoks #refer to conda documentation if you have problems with this
jupyter lab
```

## Alteranative procedure for Windows users who do not want to use WSL

A simple workaround procedure for Windows without using WSL:

- [Install Larch](https://xraypy.github.io/xraylarch/installation.html)
- Activate the Larch environment in a terminal (like you would do for executing Larch from the terminal)
- Install `famewoks` there

```
pip install git+https://gitlab.esrf.fr/F-CRG/fames/famewoks.git
```

- Open a JupyterLab session

```bash
jupyter lab
```
