# Quickstart

To quickly start using `famewoks` for analyzing your data there are three
different scenarios:

## You are doing an experiment on FAME

- Simply connect to `http://jade:8000` and open the notebook in the `SCRIPTS`
  directory of your experiment (usually at
  `/data/visitor/your_proposal/bm16/your_session/`)
- If the notebook is not there, please ask your local contact to create one from
  a previous experiment or download there the *Data reduction notebook* from
  [https://gitlab.esrf.fr/F-CRG/fames/famewoks](https://gitlab.esrf.fr/F-CRG/fames/famewoks)
- Depending on the experiment, you may adapt the names of the counters. Ask your
  local contact to get this information.

## Your experiment is finished and your data are not archived yet

- Follow the [ESRF installation procedure](./install_esrf.md)

## You want to work locally on your machine

- Follow the [local installation procedure](./install_local.md)
- If you do not have already your own notebook, download the *Data reduction notebook* from
  [https://gitlab.esrf.fr/F-CRG/fames/famewoks](https://gitlab.esrf.fr/F-CRG/fames/famewoks)