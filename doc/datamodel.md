# Data model

Famewoks connects BLISS to Larch. It reads raw data collected at the beamline
in HDF5 format and permits the users to visualize, filter, merge, and save the
data into an Athena project file for further analysis using Larch/Larix GUI.

```{figure} ./figs/data_model-v24_5.png
:name: fig_datamodel
:alt: Data Model
:align: center
:width: 90%

Data model.
```

The data model, that is how the data from disk are mapped in memory, is shown
in {numref}`fig_datamodel`. All data structures are represented by Python
standard `dataclasses`, as they provide a clear and concise way to define the
data structures and permits validation.

The data are organized in a tree-like structure (`famewoks.datamodel`):

- Experimental session (`ExpSession`)
  - -> sample (`ExpSample`)
    - -> dataset (`ExpDataset`)
      - -> scan (`XasScanGroup`)

The base data container is a Larch `Group`.
