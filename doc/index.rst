.. Famewoks documentation master file, created by
   sphinx-quickstart on Tue Jun  4 11:24:07 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Famewoks
=========

This project is primarily targeted to the users of the FAME beamlines,
`FAME-UHD
<https://www.esrf.fr/home/UsersAndScience/Experiments/CRG/BM16.html>`_ and
`FAME-PIX
<https://www.esrf.fr/home/UsersAndScience/Experiments/CRG/BM30.html>`_, the
French CRG X-ray spectroscopy beamlines at the ESRF. The goal is to help applying
common data reduction/analysis workflows at the beamline.

The code should be compatible with any data collected at other ESRF beamlines
with the `BLISS <https://bliss.gitlab-pages.esrf.fr/bliss/master/>`_ control
software. But it has not been tested with such datasets yet.

The project is under **active development** and my be affected by bugs. Please,
subscribe to the `fame-data-analysis@esrf.fr` mailing list `here
<https://sympa.esrf.fr/sympa/info/fame-data-analysis>`_ to be kept updated
about bug fixes and new features. Report bugs and features requests directly in
the `famewoks issues tracker
<https://gitlab.esrf.fr/F-CRG/fames/famewoks/-/issues>`_ or by directly sending
an email to `mauro.rovezzi@esrf.fr`.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   quickstart
   installation
   datamodel
   notebooks/index
   modules/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
