# ESRF installation (remote)

It is possible to use the `famewoks` environment at the ESRF via the
`jupyter-slurm.esrf.fr` portal by following this simple procedure (once):

- Login into [https://jupyter-slurm.esrf.fr](https://jupyter-slurm.esrf.fr)
- Go to the **Advanced tab**
- Choose **Jupyter environment: Custom** with the following parameters:
  - Environment name: `famewoks`
  - Path: `/home/esrf/rovezzi/local/miniforge/envs/famewoks/bin`
  - Click on *Add custom environment*

Once this procedure is completed. You can start your server choosing the
`famewoks` environment directly from the **Jupyter environment** combo box in
the *Simple* tab.

## Frequently asked questions (FAQ)

### How do I access `/data/visitor` from Jupyter(Lab)?

The Jupyter notebook/lab sees only your home directory
(`/home/esrf/your_username`), thus if you want to open a notebook that is
stored in your experiment data at `/data/visitor/your_proposal` you need to
**create a symbolic link to this directory in your home**. This can be easily
done by opening a terminal (*File -> New -> Terminal*) and then:

```bash
$ ln -s /data/visitor/your_proposal MYDATA
```

You will then see a `MYDATA` directory in your home that permits you accessing
your experiment's data. **NOTE**: this procedure works for accessing any other
mount point available in the ESRF network (e.g. `/data/inhouse` or
`/tmp_14_days` workspaces).

### Why my kernel in JupyterLab is called `Python 3 (ipykernel)`?

Once you launch the JupyterLab server with the `famewoks` Python environment,
the kernel name that is showed (usually at the top right corner) is the one
defined by default, that is `Python 3 (ipykernel)`. This is because the
`famewoks` environment does not define a custom kernel. On `jupyter-slurm` you
do now have to choose a specific kernel, as it is on the beamline. Simply go
ahead and everything is fine.

### What to do if I have a `PermissionError`?

It may happen that you are trying to read/write data from an experiment as
unauthorized user. In this case you will have a `PermissionError: [Errno 13]
Permission denied: ...` in the output trace. This can be solved by asking to the
main proposer of the experiment (or the local contact) to grant access to your
user account via the [https://data.esrf.fr/](https://data.esrf.fr/) portal.